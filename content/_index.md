---
layout: index-lista
---

O Projeto de Lei 2630 de 2020, de autoria do senador Alessandro Vieira, foi à votação na Sessão Deliberativa Remota (SDR) nº 62 no dia 30 de junho de 2020 às 16h. Desde a sua concepção, o projeto foi alvo de críticas de diversas entidades e pessoas, pois previa a vigilância em massa da população, que, nas mãos de políticos, serviria para legalizar a perseguição política de seus adversários.


Nas primeiras versões do relatório do senador Angelo Coronel, o texto da lei previa a obrigatoriedade da identificação de usuários de redes sociais, a limitação da quantidade de encaminhamentos e de capacidade de grupos, uma vaga definição de desinformação e fake news, além de criminalizar contas automatizadas (bots) e ferramentas externas para disparos em massa, o que abria uma enorme brecha para cercear os direitos e liberdades na internet, que nos foram garantidos pela Constituição Federal.


Mesmo sendo alvo de críticas duras, tanto por parte de entidades como a Coalizão Direitos na Rede, Internet Lab, CGI.br, Electronic Frontier Foundation e das gigantes Google e Facebook, o projeto foi aprovado na SDR 62 de 30 de junho de 2020. Além de apontar as regras controversas e perigosas da lei, o que as entidades e a população pediam era algo simples: mais tempo para apreciação do projeto, para que todos pudessem opinar e participar da construção de um texto sólido que combate a desinformação, ao mesmo tempo que não restringe ou retira nossos direitos na internet. **Infelizmente**, os senadores decidiram **ignorar** as solicitações da sociedade e prosseguir com a votação, com o presidente do Senado Federal Davi Alcolumbre dizendo na SDR que não havia razões procedimentais para não votar o texto do PL 2630.